import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import junit.framework.Assert;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Example5 {

    WebDriver driver;

    @Before
    public void tearUp() {
        driver = new FirefoxDriver();
    }

    //@After
    //public void tearDown() { driver.close(); }

    @Test
    public void test() {
        driver.get("http://10.72.124.151:3000");
        driver.findElement(By.linkText("Log in")).click();
        driver.findElement(By.id("login")).sendKeys("rafalima");
        driver.findElement(By.id("password")).sendKeys("password");
        driver.findElement(By.name("commit")).click();
        driver.findElement(By.linkText("Administration interface")).click();
        driver.findElement(By.linkText("Customers")).click();
        driver.findElement(By.id("q_username")).sendKeys("rafalima");
        driver.findElement(By.name("commit")).click();
        assertThat(driver.findElement(By.className("pagination_information")).getText(),
                containsString("Displaying 1 Customer"));
        assertThat(driver.findElement(By.id("index_table_customers")).findElement(By.tagName("tbody")).findElement(By.className("col-username")).getText(),
                containsString("rafalima"));
    }
}


