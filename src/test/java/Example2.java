import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Example2 {

    WebDriver driver;

    @Before
    public void tearUp() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Test
    public void test() {
        driver.get("http://www.google.com");
        driver.findElement(By.id("lst-ib")).sendKeys("selenium");
        driver.findElement(By.linkText("Selenium - Web Browser Automation")).click();
        assertThat(driver.findElement(By.id("mainContent")).getText(),containsString("What is Selenium?"));
    }
}
