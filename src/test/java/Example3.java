import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import junit.framework.Assert;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class Example3 {

    WebDriver driver;

    @Before
    public void tearUp() {
        driver = new FirefoxDriver();
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Test
    public void test() {
        driver.get("https://docs.google.com/forms/d/1M-TceJTer1N81ov46JsdkxXN62nMiSCU5TScgCzSCNI/viewform?c=0&w=1");
        driver.findElement(By.id("entry_1050252143")).sendKeys("Marlon Almeida");
        new Select(driver.findElement(By.id("entry_2043435478"))).selectByValue("Python");
        driver.findElement(By.id("group_1094861216_1")).click();
        driver.findElement(By.id("group_1880671481_1")).click();
        driver.findElement(By.id("group_1880671481_2")).click();
        driver.findElement(By.name("submit")).submit();
    }

    @Test
    public void testFail() {
        driver.get("https://docs.google.com/forms/d/1M-TceJTer1N81ov46JsdkxXN62nMiSCU5TScgCzSCNI/viewform?c=0&w=1");
        driver.findElement(By.name("submit")).submit();
        assertThat(driver.findElement(By.className("ss-required-asterisk")).getText(),containsString("*Obrigatório"));
    }
}
