//package twseleniumworkshop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class Exercicio4 {
    WebDriver driver;

    @Before
    public void setup() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    /* O teste abaixo foi propositalmente alterado para falhar. Que tal consertar? ;)
      
      Teste para editar detalhes do usu�rio.
      1. Acessar http://www.flipkart.com/account
      2. Preencher campo de email com: bruno@gmail.com
      3. Preencher campo de senha com: Tartaruga01
      4. Clicar no bot�o de login
      5. Verificar se o usu�rio realizou login com sucesso
    */


    // EXERC�CIO ANULADO
    @Test
    public void loginUsuario() {
        driver.get("http://www.flipkart.com/account");
        driver.findElement(By.className("fk-input.login-form-input.user-email")).sendKeys("bruno@gmail.com");
//        driver.getTitle().equals("login_password1");
//        driver.findElement(By.id("login-cta"));
//        String mensagem = driver.findElement(By.className("fk-font-verybig")).getText();
//
//        assertThat(mensagem, not("Login successful!"));
    }

//    public int geraNumeroRandomico() {
//        Random numeroRandomico = new Random();
//        return numeroRandomico.nextInt(999999999);
//    }

}